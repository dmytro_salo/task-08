@XmlSchema(
        namespace = "http://www.nure.ua",
        elementFormDefault = XmlNsForm.QUALIFIED)
package com.epam.rd.java.basic.task8.entities;

import jakarta.xml.bind.annotation.XmlNsForm;
import jakarta.xml.bind.annotation.XmlSchema;