package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private final Document document;

	public DOMController(String xmlFileName) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		documentBuilderFactory.setFeature("http://xml.org/sax/features/validation", true);
		documentBuilderFactory.setFeature("http://apache.org/xml/features/validation/schema", true);

		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		this.document = documentBuilder.parse(xmlFileName);
	}

	public Flowers parseFlowers() {
		Element root = document.getDocumentElement();
		Flowers flowers = new Flowers();

		NodeList questionNodes = root.getElementsByTagName("flower");
		for (int j = 0; j < questionNodes.getLength(); j++) {
			if (questionNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
				Flower flower = parseFlower((Element) questionNodes.item(j));
				flowers.addFlower(flower);
			}
		}

		return flowers;
	}

	private Flower parseFlower(Element flowerElement) {
		Flower flower = new Flower();

		{
			NodeList nodeList = flowerElement.getElementsByTagName("name");
			if (nodeList.getLength() != 0) {
				flower.setName(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = flowerElement.getElementsByTagName("soil");
			if (nodeList.getLength() != 0) {
				flower.setSoil(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = flowerElement.getElementsByTagName("origin");
			if (nodeList.getLength() != 0) {
				flower.setOrigin(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = flowerElement.getElementsByTagName("multiplying");
			if (nodeList.getLength() != 0) {
				flower.setMultiplying(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = flowerElement.getElementsByTagName("visualParameters");
			if (nodeList.getLength() != 0) {
				flower.setVisualParameters(parseVisualParameters((Element) nodeList.item(0)));
			}
		}

		{
			NodeList nodeList = flowerElement.getElementsByTagName("growingTips");
			if (nodeList.getLength() != 0) {
				flower.setGrowingTips(parseGrowingTips((Element) nodeList.item(0)));			}
		}

		return flower;
	}

	private VisualParameters parseVisualParameters(Element visualParametersElement) {
		VisualParameters visualParameters = new VisualParameters();

		{
			NodeList nodeList = visualParametersElement.getElementsByTagName("stemColour");
			if (nodeList.getLength() != 0) {
				visualParameters.setStemColour(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = visualParametersElement.getElementsByTagName("leafColour");
			if (nodeList.getLength() != 0) {
				visualParameters.setLeafColour(nodeList.item(0).getTextContent());
			}
		}

		{
			NodeList nodeList = visualParametersElement.getElementsByTagName("aveLenFlower");
			if (nodeList.getLength() != 0) {
				VisualParameters.AveLenFlower aveLenFlower = new VisualParameters.AveLenFlower();

				Element aveLenFlowerElement = (Element) nodeList.item(0);
				aveLenFlower.setContent(Integer.parseInt(aveLenFlowerElement.getTextContent()));
				aveLenFlower.setMeasure(aveLenFlowerElement.getAttribute("measure"));

				visualParameters.setAveLenFlower(aveLenFlower);
			}
		}

		return visualParameters;
	}

	private GrowingTips parseGrowingTips(Element growingTipsElement) {
		GrowingTips growingTips = new GrowingTips();

		{
			NodeList nodeList = growingTipsElement.getElementsByTagName("tempreture");
			if (nodeList.getLength() != 0) {
				GrowingTips.Temperature temperature = new GrowingTips.Temperature();

				Element temperatureElement = (Element) nodeList.item(0);
				temperature.setContent(Integer.parseInt(temperatureElement.getTextContent()));
				temperature.setMeasure(temperatureElement.getAttribute("measure"));

				growingTips.setTemperature(temperature);
			}
		}

		{
			NodeList nodeList = growingTipsElement.getElementsByTagName("lighting");
			if (nodeList.getLength() != 0) {
				GrowingTips.Lighting lighting = new GrowingTips.Lighting();

				Element lightingElement = (Element) nodeList.item(0);
				lighting.setLightRequiring(lightingElement.getAttribute("lightRequiring"));

				growingTips.setLighting(lighting);
			}
		}

		{
			NodeList nodeList = growingTipsElement.getElementsByTagName("watering");
			if (nodeList.getLength() != 0) {
				GrowingTips.Watering watering = new GrowingTips.Watering();

				Element wateringElement = (Element) nodeList.item(0);
				watering.setContent(Integer.parseInt(wateringElement.getTextContent()));
				watering.setMeasure(wateringElement.getAttribute("measure"));

				growingTips.setWatering(watering);
			}
		}

		return growingTips;
	}

}
