package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;

import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers domFlowers = domController.parseFlowers();

		// sort (case 1)
		domFlowers.getFlowers().sort(Comparator.comparing(Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		Utils.extractFlowerXml(outputXmlFile, domFlowers);
		Utils.validateFlower(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		Flowers saxFlowers = saxController.parseFlowers();

		// sort  (case 2)
		saxFlowers.getFlowers().sort(Comparator.comparing(Flower::getOrigin));

		// save
		outputXmlFile = "output.sax.xml";
		Utils.extractFlowerXml(outputXmlFile, saxFlowers);
		Utils.validateFlower(outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		Flowers staxFlowers = staxController.parseFlowers();


		// sort  (case 3)
		staxFlowers.getFlowers().sort(Comparator.comparing(Flower::getSoil));

		// save
		outputXmlFile = "output.stax.xml";
		Utils.extractFlowerXml(outputXmlFile, staxFlowers);
		Utils.validateFlower(outputXmlFile);
	}

}
