package com.epam.rd.java.basic.task8.entities;

import jakarta.xml.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "flowers")
@XmlAccessorType(XmlAccessType.FIELD)
public class Flowers {
    @XmlElement(name = "flower") private final List<Flower> flowers;

    public Flowers() {
        flowers = new ArrayList<>();
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flowers=" + flowers +
                '}';
    }

}
