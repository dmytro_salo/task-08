package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Controller for StAX parser.
 */
public class STAXController {
	private final String xmlFileName;
	private final Flowers flowers;

	private Flower flower;

	private VisualParameters visualParameters;
	private VisualParameters.AveLenFlower aveLenFlower;

	private GrowingTips growingTips;
	private GrowingTips.Temperature temperature;
	private GrowingTips.Lighting lighting;
	private GrowingTips.Watering watering;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowers = new Flowers();
	}

	public Flowers parseFlowers() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			if (event.isStartElement()) {
				StartElement element = event.asStartElement();
				String localName = element.getName().getLocalPart();

				if ("flower".equals(localName)) {
					flower = new Flower();
				}

				if ("visualParameters".equals(localName)) {
					visualParameters = new VisualParameters();
				}

				if ("aveLenFlower".equals(localName)) {
					aveLenFlower = new VisualParameters.AveLenFlower();
					aveLenFlower.setMeasure(element.getAttributeByName(QName.valueOf("measure")).getValue());
					aveLenFlower.setContent(Integer.parseInt(reader.nextEvent().asCharacters().getData().trim()));
				}

				if ("growingTips".equals(localName)) {
					growingTips = new GrowingTips();
				}

				if ("tempreture".equals(localName)) {
					temperature = new GrowingTips.Temperature();
					temperature.setMeasure(element.getAttributeByName(QName.valueOf("measure")).getValue());
					temperature.setContent(Integer.parseInt(reader.nextEvent().asCharacters().getData().trim()));
				}

				if ("lighting".equals(localName)) {
					lighting = new GrowingTips.Lighting();
					lighting.setLightRequiring(element.getAttributeByName(QName.valueOf("lightRequiring")).getValue());
					growingTips.setLighting(lighting);
				}

				if ("watering".equals(localName)) {
					watering = new GrowingTips.Watering();
					watering.setMeasure(element.getAttributeByName(QName.valueOf("measure")).getValue());
					watering.setContent(Integer.parseInt(reader.nextEvent().asCharacters().getData().trim()));
				}

				if ("name".equals(localName)) {
					flower.setName(reader.nextEvent().asCharacters().getData().trim());
				}

				if ("soil".equals(localName)) {
					flower.setSoil(reader.nextEvent().asCharacters().getData().trim());
				}

				if ("origin".equals(localName)) {
					flower.setOrigin(reader.nextEvent().asCharacters().getData().trim());
				}

				if ("stemColour".equals(localName)) {
					visualParameters.setStemColour(reader.nextEvent().asCharacters().getData().trim());
				}

				if ("leafColour".equals(localName)) {
					visualParameters.setLeafColour(reader.nextEvent().asCharacters().getData().trim());
				}

				if ("multiplying".equals(localName)) {
					flower.setMultiplying(reader.nextEvent().asCharacters().getData().trim());
				}

			}

			if (event.isEndElement()) {
				EndElement element = event.asEndElement();
				String localName = element.getName().getLocalPart();

				if ("flower".equals(localName)) {
					flowers.addFlower(flower);
				}

				if ("visualParameters".equals(localName)) {
					flower.setVisualParameters(visualParameters);
				}

				if ("aveLenFlower".equals(localName)) {
					visualParameters.setAveLenFlower(aveLenFlower);
				}

				if ("growingTips".equals(localName)) {
					flower.setGrowingTips(growingTips);
				}

				if ("tempreture".equals(localName)) {
					growingTips.setTemperature(temperature);
				}

				if ("watering".equals(localName)) {
					growingTips.setWatering(watering);
				}

			}

		}

		return flowers;
	}

}