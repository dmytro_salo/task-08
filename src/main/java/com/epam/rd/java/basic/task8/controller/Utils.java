package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flowers;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public abstract class Utils {

    public static void extractFlowerXml(String outputXmlFile, Flowers flowers) throws JAXBException {
        File file = new File(outputXmlFile);
        JAXBContext jaxbContext = JAXBContext.newInstance(Flowers.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.nure.ua input.xsd ");

        jaxbMarshaller.marshal(flowers, file);
    }

    public static void validateFlower(String xmlFileName) throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source schemaFile = new StreamSource(new File("input.xsd"));
        Schema schema = factory.newSchema(schemaFile);

        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new File(xmlFileName)));
    }

}
