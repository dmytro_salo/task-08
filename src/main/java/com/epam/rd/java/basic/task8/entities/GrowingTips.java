package com.epam.rd.java.basic.task8.entities;

import jakarta.xml.bind.annotation.*;

@XmlRootElement(name = "growingTips")
@XmlAccessorType(XmlAccessType.FIELD)
public class GrowingTips {
    @XmlElement(name = "tempreture") private Temperature temperature;
    private Lighting lighting;
    private Watering watering;

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }

    @XmlRootElement(name = "tempreture")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Temperature {
        @XmlValue private int content;
        @XmlAttribute private String measure;

        public int getContent() {
            return content;
        }

        public void setContent(int content) {
            this.content = content;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "Temperature{" +
                    "content=" + content +
                    ", measure='" + measure + '\'' +
                    '}';
        }
    }

    @XmlRootElement(name = "lighting")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Lighting {
        @XmlAttribute private String lightRequiring;

        public String getLightRequiring() {
            return lightRequiring;
        }

        public void setLightRequiring(String lightRequiring) {
            this.lightRequiring = lightRequiring;
        }

        @Override
        public String toString() {
            return "Lighting{" +
                    "lightRequiring='" + lightRequiring + '\'' +
                    '}';
        }

    }

    @XmlRootElement(name = "watering")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Watering {
        @XmlValue private int content;
        @XmlAttribute private String measure;

        public int getContent() {
            return content;
        }

        public void setContent(int content) {
            this.content = content;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "Watering{" +
                    "content=" + content +
                    ", measure='" + measure + '\'' +
                    '}';
        }

    }
}
