package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private final String xmlFileName;
	private final Flowers flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowers = new Flowers();
	}

	public Flowers parseFlowers() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);

		factory.setFeature("http://xml.org/sax/features/validation", true);
		factory.setFeature("http://apache.org/xml/features/validation/schema", true);

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);

		return flowers;
	}

	private Flower flower;

	private VisualParameters visualParameters;
	private VisualParameters.AveLenFlower aveLenFlower;

	private GrowingTips growingTips;
	private GrowingTips.Temperature temperature;
	private GrowingTips.Lighting lighting;
	private GrowingTips.Watering watering;

	private String currentName;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		currentName = localName;

		if ("flower".equals(localName)) {
			flower = new Flower();
		}

		if ("visualParameters".equals(localName)) {
			visualParameters = new VisualParameters();
		}

		if ("aveLenFlower".equals(localName)) {
			aveLenFlower = new VisualParameters.AveLenFlower();
			aveLenFlower.setMeasure(attributes.getValue("measure"));
		}

		if ("growingTips".equals(localName)) {
			growingTips = new GrowingTips();
		}

		if ("tempreture".equals(localName)) {
			temperature = new GrowingTips.Temperature();
			temperature.setMeasure(attributes.getValue("measure"));
		}

		if ("lighting".equals(localName)) {
			lighting = new GrowingTips.Lighting();
			lighting.setLightRequiring(attributes.getValue("lightRequiring"));
			growingTips.setLighting(lighting);
		}

		if ("watering".equals(localName)) {
			watering = new GrowingTips.Watering();
			watering.setMeasure(attributes.getValue("measure"));
		}

	}

	@Override
	public void characters(char[] ch, int start, int length) {
		String content = new String(ch, start, length).trim();

		if (content.isEmpty()) {
			return;
		}

		if ("name".equals(currentName)) {
			flower.setName(content);
		}

		if ("soil".equals(currentName)) {
			flower.setSoil(content);
		}

		if ("origin".equals(currentName)) {
			flower.setOrigin(content);
		}

		if ("stemColour".equals(currentName)) {
			visualParameters.setStemColour(content);
		}

		if ("leafColour".equals(currentName)) {
			visualParameters.setLeafColour(content);
		}

		if ("aveLenFlower".equals(currentName)) {
			aveLenFlower.setContent(Integer.parseInt(content));
		}

		if ("tempreture".equals(currentName)) {
			temperature.setContent(Integer.parseInt(content));
		}

		if ("watering".equals(currentName)) {
			watering.setContent(Integer.parseInt(content));
		}

		if ("multiplying".equals(currentName)) {
			flower.setMultiplying(content);
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if ("flower".equals(localName)) {
			flowers.addFlower(flower);
		}

		if ("visualParameters".equals(localName)) {
			flower.setVisualParameters(visualParameters);
		}

		if ("aveLenFlower".equals(localName)) {
			visualParameters.setAveLenFlower(aveLenFlower);
		}

		if ("growingTips".equals(localName)) {
			flower.setGrowingTips(growingTips);
		}

		if ("tempreture".equals(localName)) {
			growingTips.setTemperature(temperature);
		}

		if ("watering".equals(localName)) {
			growingTips.setWatering(watering);
		}

	}

}