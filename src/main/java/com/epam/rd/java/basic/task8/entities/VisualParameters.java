package com.epam.rd.java.basic.task8.entities;

import jakarta.xml.bind.annotation.*;

@XmlRootElement(name = "visualParameters")
@XmlAccessorType(XmlAccessType.FIELD)
public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private AveLenFlower aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    @XmlRootElement(name = "aveLenFlower")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AveLenFlower {
        @XmlValue private int content;
        @XmlAttribute private String measure;

        public int getContent() {
            return content;
        }

        public void setContent(int content) {
            this.content = content;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "AveLenFlower{" +
                    "content=" + content +
                    ", measure='" + measure + '\'' +
                    '}';
        }

    }
}
